package linkedin;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.internal.WrapsDriver;

public class Place {
	//This programme clear a text area using CTRL+a , and Delete. and put the text using Javascript
	  public static void main(String[] args)
			 {
			 WebDriver driver=new FirefoxDriver();
			 driver.get("http://www.seleniumframework.com/practiceform/");
			 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			 driver.manage().window().maximize();
      
			 String text = "My super long text string to be typed into text area element";
			 WebElement element = driver.findElement(By.id("vfb-10"));
			 element.click();
			 
			 Actions actionObj = new Actions(driver);
			 actionObj.keyDown(Keys.CONTROL)
			 .sendKeys(Keys.chord("a"))
			 .sendKeys(Keys.DELETE)
			 .perform();
			 
			 ((JavascriptExecutor)driver).executeScript("arguments[0].value = arguments[1];", element, text);
			                 }

}
